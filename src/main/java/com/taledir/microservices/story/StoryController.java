package com.taledir.microservices.story;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StoryController {

	Logger logger = LoggerFactory.getLogger(StoryController.class);

	@Autowired
	StoryRepository storyRepository;

	@Autowired
	StoryProfileRepository storyProfileRepository;

	@Autowired
	StoryService storyService;

	@GetMapping("/stories/info/{owner}/{slug}")
	public StoryInfoDto retrieveStoryInfo(@PathVariable String owner, @PathVariable String slug) {

    String route = storyService.constructRoute(owner, slug);

		Story story = storyRepository.findOneByRoute(route);

		if(story == null) {
			throw new StoryNotFoundException("Story does not exist.");
		}

		StoryInfoDto storyDto = new StoryInfoDto(story.getTitle(), story.getRoute(), story.getSubtitle(),
				story.getPublicationDate(), story.getLastModifiedDate());
		return storyDto;
	}

	@GetMapping("/stories/titlebody/{owner}/{slug}")
	public StoryTitleBodyDto retrieveStoryTitleBody(@PathVariable String owner, @PathVariable String slug) {

    String route = storyService.constructRoute(owner, slug);

		StoryTitleBodyDto storyTitleBodyDto = storyRepository.findStoryTitleBodyByRoute(route);

		if(storyTitleBodyDto == null) {
			throw new StoryNotFoundException("Story does not exist.");
		}

		return storyTitleBodyDto;
	}

	@GetMapping("/stories/author/{username}")
	public Set<AuthorStoryListItemDto> retrieveWriteHistory(@PathVariable String username) {
		
		Set<AuthorStoryListItemDto> authorStories = storyRepository.findStoriesByUsername(username);
		
		//Set<Story> authorStories = storyProfile.getAuthorStories();
		
		return authorStories;
	}

	@PostMapping("/stories/create")
	public ResponseEntity<Published> createStory(
		@Valid @RequestBody PublishStoryDto publishStoryDto, BindingResult bindingResult) {

		if(bindingResult.hasErrors()) {
			if(bindingResult.getFieldError() != null) {
				throw new StoryBadRequestException(bindingResult.getFieldError().getDefaultMessage());
			}
		}

		String username = storyService.getTokenUsername(publishStoryDto.getToken());
		
		if(username == null || username.length() == 0) {
			throw new StoryBadRequestException(
				"Cannot create new story.  Please try to login first.");
		}

    String route = storyService.constructRoute(username, publishStoryDto.getSlug());

		if(storyRepository.existsByRoute(route)) {
			throw new StoryAlreadyExistsException("Unable to create story, route already exists.");
		}

    Story story = new Story(publishStoryDto.getTitle(), route, 
      publishStoryDto.getSubtitle(), publishStoryDto.getSnippet());

    Set<StoryProfile> storyAuthors = new HashSet<StoryProfile>();

    for(String authorUsername : publishStoryDto.getAuthorUsernames()) {
      StoryProfile author = storyProfileRepository.findOneByUsername(authorUsername);

      //TODO: consider creating username here, and throwing a warning for logging purposes
      if(author == null) {
        logger.error("Username does not exist in story aggregate!");
        throw new StoryBadRequestException(
          "Cannot create new story.  User Profile does not exist.");
      }
      storyAuthors.add(author);

      if(author.getUsername().equals(username)) {
        story.setOwner(author);
      }
    }
		story.setStoryAuthors(storyAuthors);

		StoryBody storyBody = new StoryBody();
		storyBody.setBody(publishStoryDto.getBody());
		story.setStoryBody(storyBody);

		Story savedStory = storyRepository.save(story);

		Published published = new Published(savedStory.getId(), 
      savedStory.getTitle(), savedStory.getRoute(),
      savedStory.getPublicationDate(),
      savedStory.getLastModifiedDate()
    );
		return ResponseEntity.ok().body(published);
	}

	@PostMapping("/stories/update")
	public ResponseEntity<Published> updateStory(
    @Valid @RequestBody PublishStoryDto publishStoryDto, BindingResult bindingResult) {
    
    if(bindingResult.hasErrors()) {
      if(bindingResult.getFieldError() != null) {
        throw new StoryBadRequestException(bindingResult.getFieldError().getDefaultMessage());
      }
    }
  
    String tokenUsername = storyService.getTokenUsername(publishStoryDto.getToken());

		if(tokenUsername == null || tokenUsername.length() == 0) {
			throw new StoryBadRequestException(
				"Cannot save changes for story.  " + 
				"Please try to login first, and verify your permissions.");
		}

    Optional<Story> storyOptional = storyRepository.findById(publishStoryDto.getId());

		if(!storyOptional.isPresent()) {
			throw new StoryNotFoundException("Unable to update, story does not exist.");
		}

    Story story = storyOptional.get();

    StoryProfile owner = story.getOwner();

    if(!owner.getUsername().equals(tokenUsername)) {
			throw new StoryBadRequestException(
				"Cannot save changes for story.  Please verify your permissions.");
    }

    String route = storyService.constructRoute(owner.getUsername(), publishStoryDto.getSlug());

    story.setTitle(publishStoryDto.getTitle());
    story.setSubtitle(publishStoryDto.getSubtitle());
    story.setRoute(route);
    story.getStoryBody().setBody(publishStoryDto.getBody());
    story.setLastModifiedDate(new Date());
    story.setSnippet(publishStoryDto.getSnippet());

    Set<StoryProfile> storyAuthors = new HashSet<StoryProfile>();

    for(String authorUsername : publishStoryDto.getAuthorUsernames()) {
      StoryProfile author = storyProfileRepository.findOneByUsername(authorUsername);

      //TODO: consider creating username here, and throwing a warning for logging purposes
      if(author == null) {
        logger.error("Username does not exist in story aggregate!");
        throw new StoryBadRequestException(
          "Cannot create new story.  User Profile does not exist.");
      }
      storyAuthors.add(author);
    }
		story.setStoryAuthors(storyAuthors);

    Story savedStory = storyRepository.save(story);

		Published published = new Published(savedStory.getId(),
      savedStory.getTitle(), savedStory.getRoute(),
      savedStory.getPublicationDate(),
      savedStory.getLastModifiedDate()
    );
		return ResponseEntity.ok().body(published);
  }
  
	@GetMapping("/stories/cards")
	public ResponseEntity<Object> retrieveStoryCards(@RequestParam(required = false) Integer offset, @RequestParam(required = false) Integer limit) {

    if(offset == null) {
      offset = 0;
    }

    if(limit == null) {
      limit = 20;
    }

    Pageable currentPage = PageRequest.of(offset, limit);

		List<StoryCardDto> storyCards = storyRepository.findStoryCards(currentPage);

		if(storyCards == null) {
			throw new StoryNotFoundException("No stories found.");
		}

    Map<String, Object> body = new HashMap<String, Object>();
    body.put("results", storyCards);

    if(offset > 0) {
      String prevPagePath = "/stories/cards?offset=" + (offset - 1) + "&limit=" + limit;
      body.put("previous", prevPagePath);
    }
    
    if(storyCards.size() == limit) {
      String nextPagePath = "/stories/cards?offset=" + (offset + 1) + "&limit=" + limit;
      body.put("next", nextPagePath);
    }

		return ResponseEntity.ok().body(body);
	}

}

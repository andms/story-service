package com.taledir.microservices.story;

import org.springframework.data.jpa.repository.JpaRepository;

public interface StoryProfileRepository extends JpaRepository<StoryProfile, Integer>{

    StoryProfile findOneByUsername(String username);

}
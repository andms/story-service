package com.taledir.microservices.story;

public class StoryCardDto {

	private String title;

  private String route;
  
  private String snippet;

  private String[] authors;

	public StoryCardDto(String title, String route, String snippet, String[] authors) {
		this.title = title;
    this.route = route;
    this.snippet = snippet;
    this.authors = authors;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getRoute() {
		return route;
	}

	public void setRoute(String route) {
		this.route = route;
	}

  public String getSnippet() {
    return snippet;
  }

  public void setSnippet(String snippet) {
    this.snippet = snippet;
  }

  public String[] getAuthors() {
    return authors;
  }

  public void setAuthors(String[] authors) {
    this.authors = authors;
  }

}
package com.taledir.microservices.story;

import javax.validation.constraints.NotEmpty;

public class StoryTitleBodyDto {

    @NotEmpty
	private String title;

	private String body;

	public StoryTitleBodyDto(String title, String body) {
		this.title = title;
		this.body = body;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

}

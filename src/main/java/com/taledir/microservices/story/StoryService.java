package com.taledir.microservices.story;

import java.util.Map;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

@Service
public class StoryService {

	@Value("${spring.security.user.name}")
  private String authServiceUsername;

  @Value("${spring.security.user.password}")
  private String authServicePassword;

	Logger logger = LoggerFactory.getLogger(StoryService.class);

	@Autowired
	StoryRepository storyRepository;

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

	protected String getTokenUsername(String token) {
		
		if(token != null && token.length() > 0) {
			Map<String, Object> responseBody = getIntrospectResponse(token);

			if(responseBody != null) {
				Object username = responseBody.get("username");
				if(username != null)  {
					return username.toString();
				}
			}
		}
		return null;
	}

	private Map<String, Object> getIntrospectResponse(String token) {
		HttpHeaders reqHeaders = new HttpHeaders();
		reqHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		reqHeaders.setBasicAuth(authServiceUsername, authServicePassword);
		
		MultiValueMap<String, String> body= new LinkedMultiValueMap<String, String>();
		body.add("token", token);
		
		HttpEntity<MultiValueMap<String, String>> requestEntity = 
			new HttpEntity<MultiValueMap<String, String>>(body, reqHeaders);

		ResponseEntity<Object> responseEntity;
		try {
			responseEntity = restTemplate().exchange(
				"http://taledir-test.com/auth/introspect", 
			HttpMethod.POST, requestEntity, Object.class);

		} catch(HttpClientErrorException e) {
			logger.error("Authorization server username or password is incorrect.  " + 
				"This will prevent authorization!");
			return null;
		} catch(RestClientException e) {
			logger.error("Authorization server hostname is not setup correctly.  " + 
				"This will prevent authorization!");
			return null;
		}
		Object oBody = responseEntity.getBody();
		ObjectMapper oMapper = new ObjectMapper();
		Map<String, Object> responseBody = 
			oMapper.convertValue(oBody, new TypeReference<Map<String, Object>>() {});
			
		return responseBody;
  }

  protected String constructRoute(String owner, String slug) {
    return new StringBuilder()
      .append("/")
      .append(owner)
      .append("/")
      .append(slug)
      .toString();
  }

}

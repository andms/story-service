package com.taledir.microservices.story;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "story_profile")
public class StoryProfile {

	@Id
	@Column(name = "id")
	private Integer id;

	@Column(name = "username", nullable = false)
	private String username;

	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "storyAuthors")
	@JsonIgnore
	Set<Story> authorStories;

  @OneToMany(
    mappedBy = "owner",
    cascade = CascadeType.ALL,
    orphanRemoval = true
  )
  Set<Story> ownerStories;

	protected StoryProfile() {

	}

	public StoryProfile(Integer id, String name, String username) {
		super();
		this.id = id;
		this.username = username;
	}

	public Integer getId() {
		return id;
	}

  public void setId(Integer id) {
      this.id = id;
  }

	public String getUsername() {
		return username;
	}

  public void setUsername(String username) {
      this.username = username;
  }

	public Set<Story> getAuthorStories() {
		return authorStories;
	}

	public void setAuthorStories(Set<Story> authorStories) {
		this.authorStories = authorStories;
	}

  public Set<Story> getOwnerStories() {
    return ownerStories;
  }

  public void setOwnerStories(Set<Story> ownerStories) {
    this.ownerStories = ownerStories;
  }

}
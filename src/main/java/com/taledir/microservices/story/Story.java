package com.taledir.microservices.story;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.PastOrPresent;

import org.hibernate.annotations.NaturalId;

@Entity
@Table(name = "story")
public class Story {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name="owner_id")
  private StoryProfile owner;

	@Column(name = "title")
	private String title;

	@NaturalId(mutable = true)
	@Column(name = "route", nullable = false, unique = true)
	private String route;

	@Column(name = "subtitle")
	private String subtitle;

	@Column(name = "snippet")
	private String snippet;

	@PastOrPresent
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "publication_date")
	private Date publicationDate;

	@PastOrPresent
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "last_modified_date")
	private Date lastModifiedDate;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(
		name = "story_author", 
		joinColumns = @JoinColumn(name = "story_id"), 
		inverseJoinColumns = @JoinColumn(name = "author_id"))
	Set<StoryProfile> storyAuthors;

	@OneToOne(mappedBy = "story", cascade = CascadeType.ALL, optional = false, fetch = FetchType.LAZY)
	private StoryBody storyBody;
	
	protected Story() {

	}

	public Story(String title, String route, String subtitle, String snippet) {
		super();
		this.title = title;
		this.route = route;
    this.subtitle = subtitle;
    this.snippet = snippet;
		this.publicationDate = new Date();
		this.lastModifiedDate = new Date();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public String getRoute() {
		return route;
	}

	public void setRoute(String route) {
		this.route = route;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSubtitle() {
		return subtitle;
	}

	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}

  public String getSnippet() {
    return snippet;
  }

  public void setSnippet(String snippet) {
    this.snippet = snippet;
  }

	public Date getPublicationDate() {
		return publicationDate;
	}

	public void setPublicationDate(Date publicationDate) {
		this.publicationDate = publicationDate;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public Set<StoryProfile> getStoryAuthors() {
		return storyAuthors;
	}

	public void setStoryAuthors(Set<StoryProfile> storyAuthors) {
		this.storyAuthors = storyAuthors;
	}

	public StoryBody getStoryBody() {
		return storyBody;
	}

  public void setStoryBody(StoryBody storyBody) {
      if (storyBody == null) {
          if (this.storyBody != null) {
              this.storyBody.setStory(null);;
          }
      }
      else {
          storyBody.setStory(this);
      }
      this.storyBody = storyBody;
  }

  public StoryProfile getOwner() {
    return owner;
  }

  public void setOwner(StoryProfile owner) {
    this.owner = owner;
  }

}
